# -*- coding: utf-8 -*-
"""
Tools to download and read climate date from the Servicio Meteorologico Nacional
https://smn.conagua.gob.mx/es/climatologia/informacion-climatologica/informacion-estadistica-climatologica


Author:
Saul Arciniega Esparza | zaul_ae@hotmail.com
Hydrogeology Group, Faculty of Engineering
National Autonomous University of Mexico (UNAM)
Mexico City
"""

# ==============================================================================
# Import modules
# ==============================================================================

import os
from urllib.request import Request, urlopen
from urllib.error import HTTPError
import numpy as np
import pandas as pd
import json

try:
    import plotly.graph_objs as go
    import plotly.offline as po
    import plotly.express as px

    from .fix_plotly_error import fix_widget_error
    fix_widget_error()

    po.init_notebook_mode()
except:
    print("Interactive tools disable!")
    print("Try to install plotly using '$pip install plotly'")


PATH = os.path.dirname(os.path.abspath(__file__))

with open(os.path.join(os.path.join(PATH, 'data', 'token.txt'))) as fid:
    TOKEN = fid.read()

PROP_NAMES = [
    'Estacion',
    'Nombre',
    'Estado',
    'Municipio',
    'Situacion',
    'Organismo',
    'CVE-OMM',
    'Latitud',
    'Longitud',
    'Altitud',
    'Emision'
]


# ==============================================================================
# Main class
# ==============================================================================

class SMNdatabase(object):

    def __init__(self, path=None):
        self.url = r'https://smn.conagua.gob.mx/tools/RESOURCES/Diarios/'
        self.stations = pd.read_csv(os.path.join(PATH, 'data', 'SMN_list.csv'))
        self.stations.index = [str(x) for x in self.stations.loc[:, 'Code']]
        self._interactive_map = None

        if path is None:
            path = os.path.abspath(".")
        if not os.path.exists(path):
            os.makedirs(path)
        self.path = path
        self._map_stations = self.stations.index.values

    def set_url(self, url):
        """Change the url of the online data base"""
        if type(url) is str:
            self.url = url
        else:
            raise TypeError('url must be a string!')

    def stations_info(self):
        """Returns a DataFrame of the available gauges"""
        return self.stations.copy()

    def filter_stations(self, extent=None, state=None, rh=None, rha=None):
        """
        Filter gauges id using coordinates or locations

        Inputs:
            extent:     [list, tuple, array] rectangle coordinates
                            [xmin, xmax, ymin, ymax]
            state:      [string] state name
            rh:         [string] hydrological region
            rha:        [string] hydrological administrative region
        Outputs:
            stations:   [list] stations id contained within the input region
        """
        if type(extent) in (list, tuple, np.ndarray):
            xmin, xmax, ymin, ymax = extent
            xmin, xmax = min(xmin, xmax), max(xmin, xmax)
            ymin, ymax = min(ymin, ymax), max(ymin, ymax)
            mask = ((self.stations.X >= xmin) & (self.stations.X <= xmax) \
                & (self.stations.Y >= ymin) & (self.stations.Y <= ymax) )
            if mask.sum() > 0:
                return list(self.stations.index.values[mask.values])

            else:
                return []

        if type(state) is str:
            mask = self.stations.State.str.lower() == state.lower()
            if mask.sum() > 0:
                return list(self.stations.index.values[mask.values])

            else:
                return []

        if type(rh) is str:
            mask = self.stations.RH.str.lower() == rh.lower()
            if mask.sum() > 0:
                return list(self.stations.index.values[mask.values])

            else:
                return []

        if type(rha) is str:
            mask = self.stations.RHA.str.lower() == rha.lower()
            if mask.sum() > 0:
                return list(self.stations.index.values[mask.values])

            else:
                return []

        return []

    def _download_stations(self, codes, verbose=False):
        """
        Download txt files with the daily climatology

        Inputs:
            codes:      [list, tuple, ndarray] list of gauges id to download
            verbose:    [bool] if True, prints all the errors during download
        Outputs:
            flag        [int] returns 0 if all downloads were success.
                         flag > 0 indicates the number of fail downloads
        """
        folder = self.path
        if type(codes) is str or type(codes) is int:
            codes = [str(codes)]
        codes = [str(code) for code in codes]

        if not os.path.exists(folder):
            os.makedirs(folder)

        flag = 0
        print('Downloading files...')
        for i in range(len(codes)):
            if codes[i] not in self.stations.index.values:
                print('Error downloading: ', codes[i])
                if verbose:
                    print(f'{codes[i]} is not a valid id')
                flag += 1
                continue

            url_filename = os.path.join(self.url, codes[i] + '.txt')
            saveas = os.path.join(folder, codes[i] + '.txt')
            try:
                req = Request(url_filename, headers={'User-Agent': 'Edge'})
                data = urlopen(req).read()
                with open(saveas, 'w', encoding='utf-8') as fout:
                    fout.write(data.decode('ISO-8859-1'))
            except HTTPError as e:
                print('Error downloading: ', codes[i])
                if verbose:
                    print(e)
                flag += 1
        print('Finish!')
        return flag

    def _download_selected_stations(self, verbose=False):
        """
        Download txt files with the daily climatology of selected stations
        from the interactive map

        Inputs:
            verbose:    [bool] if True, prints all the errors during download
        Outputs:
            flag        [int] returns 0 if all downloads were success.
                         flag > 0 indicates the number of fail downloads
        """
        folder = self.path
        selected = self.selection()
        flag = 0
        if len(selected) > 0:
            flag = self._download_stations(selected, folder, verbose=verbose)
        else:
            print('Any station was selected!')
            flag = 9999
        return flag

    def load_station(self, code=None, var=None, download=False, verbose=True):
        """
        Read a txt file downloaded from the SMN database

        :param code:       [int, string] station code to read
        :param var:        [string] variable to read ('prec', 'tmin', 'tmax', 'evap')
                            If None (default), all variables are readed
        :return:           [DataFrame]
        """
        if code is None:
            if len(self.selection()) == 0:
                return [], []
            code = self.selection()[0]
        filename = os.path.join(self.path, "{}.txt".format(code))
        if download:
            self._download_stations([code], verbose=verbose)
        props, data = [], []
        if os.path.exists(filename):
            if self._is_empty(filename):
                print(f"Warning: File empty > {code}.txt")
                return props, data
            try:
                with open(filename, 'r', encoding='utf-8') as fid:
                    props, data = self._read_data(fid, var)
            except UnicodeDecodeError:
                with open(filename, 'r', encoding='ISO-8859-1') as fid:
                    props, data = self._read_data(fid, var)
        return props, data

    def load_stations(self, codes=None, var='prec', download=False, verbose=True):
        """
        Read multiple txt files downloaded from the SMN database

        :param folder:     [string] input folder where txt are located
        :param codes:      [tuple, list] list of stations to read. If None (default), all stations are readed.
        :param var:        [string] variable to read ('prec', 'tmin', 'tmax', 'evap')
        :return:           [DataFrame]
        """
        folder = self.path
        if not os.path.exists(folder):
            return [], []
        if codes is None:
            if len(self.selection()) == 0:
                return [], []
            codes = self.selection()
        if download:
            self._download_stations(codes, verbose=verbose)
        files_list = []
        for fname in codes:
            if os.path.exists(os.path.join(folder, f'{fname}.txt')):
                files_list.append(f'{fname}.txt')
        if len(files_list) == 0:
            return [], []
        # read and merge data
        code_list, props_list, data_list = [], [], []
        for fname in files_list:
            code = fname.split('.')[0]
            filename = os.path.join(folder, fname)
            if not os.path.exists(filename):
                print(f"Warning: File not in folder > {code}.txt")
                continue
            if self._is_empty(filename):
                print(f"Warning: File empty > {code}.txt")
                continue
            try:
                with open(filename, 'r', encoding='utf-8') as fid:
                    props, data = self._read_data(fid, var)
                    if len(props) == 0 or len(data) == 0:
                        continue
                    props_list.append(props)
                    data_list.append(data)
            except UnicodeDecodeError:
                with open(filename, 'r', encoding='ISO-8859-1') as fid:
                    props, data = self._read_data(fid, var)
                    if len(props) == 0 or len(data) == 0:
                        continue
                    props_list.append(props)
                    data_list.append(data)

            code_list.append(code)

        properties = pd.concat(props_list, axis=1)
        properties.columns = code_list
        properties = properties.transpose()
        dataset = pd.concat(data_list, axis=1)
        dataset.columns = code_list
        dataset.index.name = 'Date'
        return properties, dataset

    def request_station(self, code=None, var=None):
        """
        Read a station data from the online SMN database

        :param filename:   [string] input full txt file
        :param var:        [string] variable to read ('prec', 'tmin', 'tmax', 'evap')
                            If None (default), all variables are read
        :return:           [DataFrame]
        """
        if code is None:
            if len(self.selection()) == 0:
                return [], []
            code = self.selection()[0]
        code = str(code)
        url_filename = os.path.join(self.url, code + '.txt')
        try:
            req = Request(url_filename, headers={'User-Agent': 'Edge'})
            connection = urlopen(req)
            props, data = self._read_data(connection, var, decode='ISO-8859-1')
            connection.close()
            return props, data
        except HTTPError as e:
            print('Error downloading: ', code)
            print(e)
        return [], []

    def request_stations(self, codes, var='prec'):
        """
        Read multiple stations data from the online SMN database

        Inputs:
            codes:      [list, tuple, ndarray] list of gauges id to download
            verbose:    [bool] if True, prints all the errors during download
        Outputs:
            flag        [int] returns 0 if all downloads were success.
                         flag > 0 indicates the number of fail downloads
        """
        if codes is None:
            if len(self.selection()) == 0:
                return [], []
            codes = self.selection()
        codes = [str(code) for code in codes]
        # read and merge data
        code_list, props_list, data_list = [], [], []
        for code in codes:
            props, data = self.request_station(code, var)
            if len(props) == 0 or len(data) == 0:
                continue
            props_list.append(props)
            data_list.append(data)
            code_list.append(code)

        if len(props_list) > 0:
            properties = pd.concat(props_list, axis=1)
            properties.columns = code_list
            properties = properties.transpose()
            dataset = pd.concat(data_list, axis=1)
            dataset.columns = code_list
            dataset.index.name = 'Date'
            return properties, dataset
        else:
            return [], []

    def _is_empty(self, filename):
        return os.path.getsize(filename) == 0

    def _read_data(self, fid, var=None, decode=None):
        varid = 0
        varnames = ['Prec', 'Evap', 'Tmax', 'Tmin']
        if type(var) is str:
            if var.title() in varnames:
                varid = varnames.index(var.title())

        cnt = 0
        dates, data, props = [], [], []
        while True:
            cnt += 1
            if decode is None:
                line = fid.readline()
            else:
                line = fid.readline().decode(decode)

            if len(line) == 0:
                break

            if cnt > 3 and cnt <= 17:
                line = line.strip()
                if len(line) == 0:
                    continue
                props.append(line.split(':')[1].strip())

            elif cnt > 19:
                line = line.strip()
                if len(line) == 0:
                    continue
                if '----' in line:
                    continue
                vector_line = line.split()
                if len(vector_line) < 5:
                    continue
                dates.append(vector_line[0].strip())
                if var is None:
                    for i in range(1, len(vector_line)):
                        if vector_line[i] == 'Nulo':
                            vector_line[i] = np.nan
                        else:
                            vector_line[i] = float(vector_line[i])
                    data.append(vector_line[1:])
                else:
                    if vector_line[varid+1] == 'Nulo':
                        data.append(np.nan)
                    else:
                        data.append(float(vector_line[varid+1]))

        if len(props) > 0:
            props = pd.Series(props, index=PROP_NAMES[:len(props)])
        if len(data) > 0:
            dates = pd.to_datetime(dates, dayfirst=True)
            fixed_dates = self._dates_range(dates.min(), dates.max())
            if var is None:
                data1 = pd.DataFrame(
                    data,
                    index=dates,
                    columns=varnames
                    )
                data = pd.DataFrame(
                    np.full((len(fixed_dates), 4), np.nan),
                    index=fixed_dates,
                    columns=varnames
                )
                data.loc[data1.index, :] = data1

            else:
                data1 = pd.DataFrame(
                    data,
                    index=dates,
                    columns=[varnames[varid]]
                    )
                data = pd.DataFrame(
                    np.full(len(fixed_dates), np.nan),
                    index=fixed_dates,
                    columns=[varnames[varid]]
                )
                data.loc[data1.index, :] = data1

        # convert properties
        try:
            props.Altitud = float(props.Altitud.replace("msnm", "").replace(" ", "").replace(",", ""))
        except ValueError:
            props.Altitud = np.nan
        try:
            props.Latitud = float(props.Latitud.replace("°", ""))
            props.Longitud = float(props.Longitud.replace("°", ""))
        except ValueError:
            props.Latitud = np.nan
            props.Longitud = np.nan
        try:
            props.Emision = pd.Timestamp(props.Emision)
        except ValueError:
            props.Emision = np.nan

        return props, data

    def map(self, codes=None, on_folder=False, layer=None, width=None, height=None):
        """
        Show an interactive map with the location of stations

        Parameters
        ----------
        codes:       [list, tuple, array] Visualize only stations in the input list
                       It has preference over the on_folder option
        on_folder:   [bool] If True, shows only the climatological
                       stations in the database folder
        layer:       [str, dict] geojson with polygons to visualize in the map
        width:       [int] figure width in pixels
        height       [int] figure height in pixels

        Returns      plotly.Scattermap
        -------

        """
        if codes is not None:
            data = self.stations.copy()

            validated_codes = []
            for code in codes:
                if str(code) in data.index:
                    validated_codes.append(str(code))
            data = data.loc[validated_codes, :]
        else:
            if on_folder:
                data = self.stations
                codes = []
                for fname in os.listdir(self.path):
                    code = fname.split(".")[0]
                    if code in data.index:
                        codes.append(code)
                if len(codes) == 0:
                    data = self.stations
                else:
                    data = data.loc[codes, :]
            else:
                data = self.stations.copy()

        self._map_stations = data.index.values

        fig = px.scatter_mapbox(
            data,
            lat="Y",
            lon="X",
            hover_name="Code",
            hover_data=["Code", "State", "RH", "RHA"],
            color_discrete_sequence=["blue"],
            zoom=4,
            width=width,
            height=height
        )
        fig.update_layout(
            clickmode="event+select",
            mapbox=dict(
                accesstoken=TOKEN,
            ),
        )
        fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})

        # Add user layer
        if layer is not None:

            if type(layer) is str:
                with open(layer) as fid:
                    layer_json = json.load(fid)

            mapbox_layers = [
                {
                    "sourcetype": "geojson",
                    "source": layer_json,
                    "type": "line",
                    "line": {
                        "width": 2,
                    }
                }
            ]
            fig["layout"]["mapbox"]["layers"] = mapbox_layers

        fig = go.FigureWidget(fig)
        self._interactive_map = fig
        return self._interactive_map

    def selection(self):
        """
        Returns the list of codes of selected stations in the interactive map.
        If any station is selected, an empty list is returned
        """
        indices = []
        if self._interactive_map is not None:
            if self._interactive_map.data[0].selectedpoints is not None:
                idx = list(self._interactive_map.data[0].selectedpoints)
                indices = list(self._map_stations[idx])
        return indices

    @staticmethod
    def _dates_range(date_start, date_end):
        return pd.date_range(
            start=date_start,
            end=date_end,
            freq='1d'
            )

